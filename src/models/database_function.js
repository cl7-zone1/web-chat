
function render_database(database,req,res, next, page){
    
    database.find({}).then((data) => { 
        data = data.map(res => res.toObject());

        res.render(page,{data});
    }).catch(err =>{
        next(err);
    });
}

function render_toObjDB(mongoose){
    return mongoose ? mongoose.toObject() : mongoose;
}

function render_list_database(list_mongoose){
    return list_mongoose.map(mongoose => mongoose.toObject());
}

module.exports = {
    render_database,
    render_toObjDB,
    render_list_database,
};
























